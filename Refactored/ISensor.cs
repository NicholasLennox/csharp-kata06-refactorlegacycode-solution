﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KataCSharpSOLIDSolution.Refactored
{
    // We create an abstraction (interface) for Alarm to depend on, now it is decoupled from SimpleSensor
    // We also can freely add new types on sensors and not worry about Alarm needing to change
    public interface ISensor
    {
        double PopNextPressurePsiValue();
    }
}
