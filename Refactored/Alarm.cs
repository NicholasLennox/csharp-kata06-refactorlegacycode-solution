﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KataCSharpSOLIDSolution.Refactored
{
    public class Alarm
    {
        private const double LowPresureThreashold = 17;
        private const double HighPresureThreshold = 21;
        // We invert the dependency for SimpleSensor to its abstraction, ISensor
        private readonly ISensor sensor;
        bool isAlarmOn = false;

        // We create a constructor to allow us to provide the type of sensor
        // We no longer need to know about implementations, because we arent creating new sensors
        // and we no longer need to change Alarm if we decide to change sensors.
        public Alarm(ISensor sensor)
        {
            this.sensor = sensor;
        }

        public void CheckPressure()
        {
            double psiPressureValue = sensor.PopNextPressurePsiValue();
            // This is fine as is, because we are using the sensor data - it is our responsibility.
            if (psiPressureValue < LowPresureThreashold || psiPressureValue > HighPresureThreshold)
            {
                isAlarmOn = true;
            }
        }

        // Instead of writing, we are returning stings to be written somewhere else 
        public string ShowDetails()
        {
            if (isAlarmOn)
            {
                return "BEEP BOOP the alarm bells are ringing! Control the pressure";
            }
            else
            {
                return "Everything is fine";
            }
        }
    }
}
