﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KataCSharpSOLIDSolution.Refactored
{
    public class SimpleSensor : ISensor
    {
        public const double OFFSET = 16;

        public double PopNextPressurePsiValue()
        {
            double pressureTelemetryValue;
            pressureTelemetryValue = SamplePressure();
            return OFFSET + pressureTelemetryValue;
        }

        // This method didnt need to be part of the ISensor interface, its SimpleSensor specific. 
        private double SamplePressure()
        {
            Random basicRandomNumbersGenerator = new Random();
            double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.NextDouble() * basicRandomNumbersGenerator.NextDouble();
            return pressureTelemetryValue;
        }
    }
}
