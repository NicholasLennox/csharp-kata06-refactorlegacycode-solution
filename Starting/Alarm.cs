﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KataCSharpSOLIDSolution.Starting
{
    public class Alarm
    {
        private const double LowPresureThreashold = 17;
        private const double HighPresureThreshold = 21;

        // OCP - we have to modify Alarm when we want to add and use new sensors
        // DIP - we have a high level module (Alarm) depending on a low level module (simple sensor)
        // it should depend on abstraction
        private readonly SimpleSensor sensor = new SimpleSensor();
        bool isAlarmOn = false;

        public void CheckPressure()
        {
            double psiPressureValue = sensor.PopNextPressurePsiValue();

            if(psiPressureValue < LowPresureThreashold || psiPressureValue > HighPresureThreshold)
            {
                isAlarmOn = true;
            }
        }

        // SRP - we shouldnt do the writing here, as this class is responsible for monitoring pressure 
        public void ShowDetails()
        {
            if (isAlarmOn)
            {
                Console.WriteLine("BEEP BOOP the alarm bells are ringing! Control the pressure");
            } else
            {
                Console.WriteLine("Everything is fine");
            }
        }
    }
}
